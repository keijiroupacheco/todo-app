Clone this repository

`npm install` for both applicationns 

Run the applications:

Frontend:
`npm start`

Backend
`npx tsc`
`node dist/app.js`

Note: I skipped using Postgres since I believe it is time-consuming to setup and overkill to this project, but instead I just used SQLite, so no need to setup the database, it is already there (todo.db)