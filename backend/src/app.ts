import express from 'express'
import sqlite3 from 'sqlite3'
import cors from "cors"

const app = express()
const port = 3100

app.use(cors())
app.use(express.json())


app.get('/', (req, res) => {
  res.send('Hello World!')
})


let db = new sqlite3.Database('./todo.db', sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, (err: any) => {
    if (err) {
        console.log("Getting error " + err)
        process.exit(1)
    }

    createDatabase()
})

function createDatabase() {
    var newDB = new sqlite3.Database('./todo.db', (err: any) => {
        createTables(newDB)
    })
}

function createTables(db: sqlite3.Database) {
    db.run(`
    CREATE TABLE IF NOT EXISTS t_todo (
        todo_idx INTEGER PRIMARY KEY AUTOINCREMENT,
        todo_text TEXT NOT NULL,
        is_completed BOOLEAN NOT NULL CHECK (is_completed IN (0, 1)) DEFAULT 0
    )
        `)

    db.run(`INSERT INTO t_todo (todo_text) VALUES (?)`, ["Make Salad"])
    
    db.close()
}


app.get("/", (req, res) => {
    res.json("hello")
  })
  
app.get("/todos", (req, res) => {
    const query = "SELECT * FROM t_todo"

    db.all(query, [], (err, data) => {
        if (err) {
                console.log(err)
                return res.json(err)
            }

            return res.json(data)
    })
})

app.post("/todo", (req, res) => {
    const query = "INSERT INTO t_todo (`todo_text`) VALUES (?)"
  
    db.run(query, [req.body.text], function (err: any) {
      if (err) return res.send(err)

      db.get("SELECT * FROM t_todo WHERE todo_idx = ?", [this.lastID], (err, row) => {
        if (err) return res.send(err)
        return res.json(row)
      })
      
    })
})
  
app.delete("/todo/:id", (req, res) => {
    const todoId = req.params.id
    const query = "DELETE FROM t_todo WHERE todo_idx = ?"
  
    db.run(query, [todoId], (err: any, data: any) => {
      if (err) return res.send(err)
      return res.json(data)
    })
})
  
app.put("/todo/:id", (req, res) => {
    const todoId = req.params.id
    const query = "UPDATE t_todo SET `todo_text`= ?, `is_completed`= ? WHERE todo_idx = ?"
  
    const values = [
      req.body.text,
      req.body.isCompleted,
    ]
  
    db.run(query, [...values, todoId], (err: any, data: any) => {
      if (err) return res.send(err)
      return res.json(data)
    })
})


app.listen(port, () => {
    return console.log(`Express is listening at http://localhost:${port}`)
})