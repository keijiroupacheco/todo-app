"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const sqlite3_1 = __importDefault(require("sqlite3"));
const cors_1 = __importDefault(require("cors"));
const app = (0, express_1.default)();
const port = 3100;
app.use((0, cors_1.default)());
app.use(express_1.default.json());
app.get('/', (req, res) => {
    res.send('Hello World!');
});
let db = new sqlite3_1.default.Database('./todo.db', sqlite3_1.default.OPEN_READWRITE | sqlite3_1.default.OPEN_CREATE, (err) => {
    if (err) {
        console.log("Getting error " + err);
        process.exit(1);
    }
    createDatabase();
});
function createDatabase() {
    var newDB = new sqlite3_1.default.Database('./todo.db', (err) => {
        createTables(newDB);
    });
}
function createTables(db) {
    db.run(`
    CREATE TABLE IF NOT EXISTS t_todo (
        todo_idx INTEGER PRIMARY KEY AUTOINCREMENT,
        todo_text TEXT NOT NULL,
        is_completed BOOLEAN NOT NULL CHECK (is_completed IN (0, 1)) DEFAULT 0
    )
        `);
    db.run(`INSERT INTO t_todo (todo_text) VALUES (?)`, ["Make Salad"]);
    db.close();
}
app.get("/", (req, res) => {
    res.json("hello");
});
app.get("/todos", (req, res) => {
    const query = "SELECT * FROM t_todo";
    db.all(query, [], (err, data) => {
        if (err) {
            console.log(err);
            return res.json(err);
        }
        return res.json(data);
    });
});
app.post("/todo", (req, res) => {
    const query = "INSERT INTO t_todo (`todo_text`) VALUES (?)";
    db.run(query, [req.body.text], function (err) {
        if (err)
            return res.send(err);
        db.get("SELECT * FROM t_todo WHERE todo_idx = ?", [this.lastID], (err, row) => {
            if (err)
                return res.send(err);
            console.log(row);
            return res.json(row);
        });
    });
});
app.delete("/todo/:id", (req, res) => {
    const todoId = req.params.id;
    const query = "DELETE FROM t_todo WHERE todo_idx = ?";
    db.run(query, [todoId], (err, data) => {
        if (err)
            return res.send(err);
        return res.json(data);
    });
});
app.put("/todo/:id", (req, res) => {
    const todoId = req.params.id;
    const query = "UPDATE t_todo SET `todo_text`= ?, `is_completed`= ? WHERE todo_idx = ?";
    const values = [
        req.body.text,
        req.body.isCompleted,
    ];
    db.run(query, [...values, todoId], (err, data) => {
        if (err)
            return res.send(err);
        return res.json(data);
    });
});
app.listen(port, () => {
    return console.log(`Express is listening at http://localhost:${port}`);
});
