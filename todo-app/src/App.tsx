// Import dependencies
import { ChangeEvent, useEffect, useState } from 'react'
import axios from 'axios'

// Import components
import TodoForm from './components/todo-form'
import TodoList from './components/todo-list'

// Import interfaces
import { TodoInterface } from './interfaces'

// Import styles
import './styles/styles.css'


// TodoListApp component
const TodoListApp = () => {
  const [todos, setTodos] = useState<TodoInterface[]>([])
  const [error, setError] = useState(false)

  const domain: string = "http://localhost:3100"

  useEffect(() => {
    const fetchAllTodos = async () => {
      try {
        const res = await axios.get(`${domain}/todos`)
        let allTodos: TodoInterface[] = []

        res.data.forEach((row: { [x: string]: any }) => {
          allTodos.push({
            id: `${row["todo_idx"]}`,
            text: `${row["todo_text"]}`,
            isCompleted: Boolean(Number(`${row["is_completed"]}`))
          })
        })

        setTodos(allTodos)
      } catch (err) {
        console.log(err)
      }
    }
    fetchAllTodos()
  }, [])

  // Creating new todo item
  async function handleTodoCreate(todo: TodoInterface) {
    // Prepare new todos state
    const newTodosState: TodoInterface[] = [...todos]

    await axios.post(`${domain}/todo`, todo).then((row: any) => {
      // Update new todos state
      newTodosState.push({
        id: `${row.data["todo_idx"]}`,
        text: `${row.data["todo_text"]}`,
        isCompleted: false
      })

      // Update todos state
      setTodos(newTodosState)
    }).catch((error) => {
      // Log the error message and code
      console.log(`Error message: ${error.message}`)
      console.log(`Error code: ${error.code}`)

      setError(true)
    })
  }

  // Update existing todo item
  function handleTodoChange(event: ChangeEvent<HTMLInputElement>, id: string) {
    // Prepare new todos state
    const newTodosState: TodoInterface[] = [...todos]

    // Find correct todo item to update
    newTodosState.find((todo: TodoInterface) => todo.id === id)!.text = event.target.value

    // Update todos state
    setTodos(newTodosState)
  }

  async function handleTodoUpdate(todo: TodoInterface) {  
    if (todo.text.length > 0)
      await axios.put(`${domain}/todo/${todo.id}`, todo).then((response) => {


      }).catch((error) => {
        // Log the error message and code
        console.log(`Error message: ${error.message}`)
        console.log(`Error code: ${error.code}`)

        setError(true)
      })
  }

  // Remove existing todo item
  async function handleTodoRemove(id: string) {
    // Prepare new todos state
    const newTodosState: TodoInterface[] = todos.filter((todo: TodoInterface) => todo.id !== id)

    await axios.delete(`${domain}/todo/${id}`).then((response) => {

      // Update todos state
      setTodos(newTodosState)
    }).catch((error) => {
      // Log the error message and code
      console.log(`Error message: ${error.message}`)
      console.log(`Error code: ${error.code}`)

      setError(true)
    })
  }

  // Check existing todo item as completed
  function handleTodoComplete(updatedTodo: TodoInterface, editState: boolean) {
    // Cannot complete while in edit mode
    if (!editState) {
      // Copy current todos state 
      const newTodosState: TodoInterface[] = [...todos]

      // Find the correct todo item and update its 'isCompleted' key
      newTodosState.find((todo: TodoInterface) => todo.id === updatedTodo.id)!.isCompleted = !newTodosState.find((todo: TodoInterface) => todo.id === updatedTodo.id)!.isCompleted

      handleTodoUpdate(updatedTodo)
      
      // Update todos state
      setTodos(newTodosState)
    }
  }

  // Check if todo item has title
  function handleTodoBlur(event: ChangeEvent<HTMLInputElement>) {
    if (event.target.value.length === 0) {
      event.target.classList.add('todo-input-error')
    } else {
      event.target.classList.remove('todo-input-error')
    }
  }

  return (
    <div className="todo-list-app">
      <h1>Todo List</h1>
      <TodoForm
        todos={todos}
        handleTodoCreate={handleTodoCreate}
      />

      <TodoList
        todos={todos}
        handleTodoChange={handleTodoChange}
        handleTodoRemove={handleTodoRemove}
        handleTodoUpdate={handleTodoUpdate}
        handleTodoComplete={handleTodoComplete}
        handleTodoBlur={handleTodoBlur}
      />
      {error && <span style={{color: "red"}}>Something Went Wrong!</span>}
    </div>
  )
}

export default TodoListApp