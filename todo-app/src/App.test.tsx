import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import App from './App';

jest.mock("axios", () => ({
  __esModule: true,

  default: {
    get: () => ({
      data: { todo_idx: 1, todo_text: "Make Salad", is_completed: false },
    }),
  },
}));


test("input should be rendered", () => {
  render(<App />);
  const inputEl = screen.getByPlaceholderText(/Enter new todo/i);
  expect(inputEl).toBeInTheDocument();
});

test("button should be rendered", () => {
  render(<App />);
  const buttonEl = screen.getByRole("button");
  expect(buttonEl).toBeInTheDocument();
});

test("button should be disabled", () => {
  render(<App />);
  const buttonEl = screen.getByRole("button");
  expect(buttonEl).toBeDisabled();
});

test("input should be empty", () => {
  render(<App />);
  const inputEl: HTMLInputElement = screen.getByPlaceholderText(/Enter new todo/i);
  expect(inputEl.value).toBe("");
});

test("input should change", () => {
  render(<App />);
  const inputEl: HTMLInputElement = screen.getByPlaceholderText(/Enter new todo/i);
  const testValue = "test";

  fireEvent.change(inputEl, { target: { value: testValue } });
  expect(inputEl.value).toBe(testValue);
});

test("button should not be disabled when inputs exist", () => {
  render(<App />);
  const buttonEl = screen.getByRole("button");
  const inputEl = screen.getByPlaceholderText(/Enter new todo/i);
  const testValue = "test";

  fireEvent.change(inputEl, { target: { value: testValue } });

  expect(buttonEl).not.toBeDisabled();
});

test("todo should be rendered after adding", async () => {
  render(<App />);
  const buttonEl = screen.getByRole("button");
  const inputEl = screen.getByPlaceholderText(/Enter new todo/i);

  const testValue = "test";

  fireEvent.change(inputEl, { target: { value: testValue } });
  fireEvent.click(buttonEl);

  const todoItem = await screen.getByDisplayValue(testValue);

  expect(todoItem).toBeInTheDocument();
});