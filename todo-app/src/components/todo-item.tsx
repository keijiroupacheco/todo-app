// Import dependencies
import React, { ChangeEvent, Fragment, useRef, useState } from 'react'

// Import interfaces
import { TodoInterface, TodoItemInterface } from './../interfaces'

// TodoItem component
const TodoItem = (props: TodoItemInterface) => {
  
  const [editState, setEditState] = useState(false)

  function editInput() {
    if (props.todo.text.length > 0 || !editState) {
      if (editState) {
        props.handleTodoUpdate(props.todo)
      }

      setEditState(!editState)
    }
  }

  return (
    <div className='todo-item'>
      <div onClick={() => props.handleTodoComplete(props.todo, editState)}>
        {props.todo.isCompleted ? (
          <span className="todo-item-checked">&#x2714;</span>
        ) : (
          <span className="todo-item-unchecked" />
        )}
      </div>

      <div className="todo-item-input-wrapper">
        <input
          value={props.todo.text}
          onBlur={props.handleTodoBlur}
          onChange={(event: ChangeEvent<HTMLInputElement>) => props.handleTodoChange(event, props.todo.id!)}
          disabled={!editState}
          style={{textDecoration: props.todo.isCompleted ? 'line-through' : 'none'}}
        />
      </div>

      <div className="item-remove" title="Edit Todo" onClick={() => editInput()}>
        {editState ? (
          <Fragment>&#x2714;</Fragment>
        ) : (
          <Fragment>&#x270F;</Fragment>
        )}
      </div>
      <div className="item-remove" title="Remove Todo" onClick={() => props.handleTodoRemove(props.todo.id!)}>
        &#x02A2F;
      </div>
    </div>
  )
}

export default TodoItem
