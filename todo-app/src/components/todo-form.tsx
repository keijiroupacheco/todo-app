// Import dependencies
import React, { ChangeEvent, KeyboardEvent, useRef, useState } from 'react'
// Import interfaces
import {TodoInterface, TodoFormInterface} from './../interfaces'

// Todo form component
const TodoForm = (props: TodoFormInterface) => {
  // Create ref for form input
  const inputRef = useRef<HTMLInputElement>(null)

  // Create new form state
  const [formState, setFormState] = useState('')

  // Prepare new todo object
  function addTodo() {
    const newTodo: TodoInterface = {
      id: '',
      text: formState,
      isCompleted: false
    }

    // Create new todo item
    if (formState.length > 0) {
      props.handleTodoCreate(newTodo)
    }

    // Reset the input field
    if (inputRef && inputRef.current) {
      setFormState('')
      inputRef.current.value = ''
    }
  }

  // Handle todo input change
  function handleInputChange(event: ChangeEvent<HTMLInputElement>) {
    // Update form state with the text from input
    setFormState(event.target.value)
  }

  // Handle 'Enter' in todo input
  function handleInputEnter(event: KeyboardEvent<HTMLInputElement>) {
    // Check for 'Enter' key
    if (event.key === 'Enter') {
      addTodo()
    }
  }

  return (
    <div className="todo-form">
      <input
        ref={inputRef}
        type="text"
        placeholder='Enter new todo'
        onChange={event => handleInputChange(event)}
        onKeyUp={event => handleInputEnter(event)}
      />
      <button onClick={addTodo} disabled={inputRef.current?.value.length === 0}>Add</button> 
    </div>
  )
}

export default TodoForm
